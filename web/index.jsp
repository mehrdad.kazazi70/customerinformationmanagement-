<%--  Created by IntelliJ IDEA.
  User: Mehrdad Kazazi
  Date: 5/29/2020
  Time: 3:27 PM
  To change this template use File | Settings | File Templates.--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div>
<ul class="nav nav-tabs">
    <li role="presentation"><a href="/person/definitionCustomer/definitionProfile.jsp">New customer definition</a></li>
    <li role="presentation"><a href="/person/searchCustomers/searchCustomers.jsp">Search Customer</a></li>
</ul>
</div>
</body>
</html>
