<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>definition Customer</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<div>
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="/person/definitionCustomer/definitionProfile.jsp">New customer definition</a></li>
        <li role="presentation"><a href="/person/searchCustomers/searchCustomers.jsp">Search Customer</a></li>
    </ul>
</div>
<div class="container">
    <form class="form-horizontal" action="/realPerson/save.do">
        <fieldset>
            <legend>RealCustomer Profile</legend>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realName">name</label>
                <div class="col-md-4">
                    <input id="realName" name="name" type="text" placeholder="name" class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realFamily">family</label>
                <div class="col-md-4">
                    <input id="realFamily" name="family" type="text" placeholder="family"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realFatherName">fatherName</label>
                <div class="col-md-4">
                    <input id="realFatherName" name="fatherName" type="text" placeholder="fatherName"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realBirthDate">birthDate</label>
                <div class="col-md-4">
                    <input id="realBirthDate" name="birthDate" type="Date" placeholder="birthDate"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
               <label class="col-md-4 control-label" for="realNationalCode">nationalCode</label>
                <div class="col-md-4">
                    <input id="realNationalCode" name="nationalCode" type="text" value="" minlength="8" maxlength="10" placeholder="nationalCode"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton1"></label>
                <div class="col-md-4">
                    <button id="singlebutton1" name="singlebutton" class="btn btn-primary">submit</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<br>
<hr>
<br>
<div class="container">
    <form class="form-horizontal" action="/saveLegal.do">
        <fieldset>
            <legend>LegalCustomer Profile</legend>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="legalName">name</label>
                <div class="col-md-4">
                    <input id="legalName" name="legalName" type="text" placeholder="name" class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="registerDate">registerDate</label>
                <div class="col-md-4">
                    <input id="registerDate" name="registerDate" type="Date" placeholder="registerDate"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="legalEconomicCode">economicCode</label>
                <div class="col-md-4">
                    <input id="legalEconomicCode" name="legalEconomicCode" type="text" value="" minlength="12" maxlength="12" placeholder="economicCode"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">submit</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>
</body>
</html>