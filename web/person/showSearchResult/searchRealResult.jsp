<%@ page import="model.entity.RealPerson" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>customer list show room</title>
    <link rel="stylesheet" href="/WEB-INF/bootstrap-4.1.3">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Family</th>
            <th>FatherName</th>
            <th>BirthDate</th>
            <th>NationalCode</th>
            <th>CustomerNumber</th>
            <th>ACTION</th>
            <th>ACTION</th>
        </tr>
        </thead>
        <tbody>
        <%
            List<RealPerson> realPerson = (List<RealPerson>) request.getAttribute("result");
            for (RealPerson person : realPerson) {
        %>
        <tr>
            <form action="/realPerson/update.do">
                <td><input type="text" class="form-control" name="realName" value="<%=person.getName()%>"/>
                </td>
                <td><input type="text" class="form-control" name="realFamily" value="<%=person.getFamily()%>"/>
                </td>
                <td><input type="text" class="form-control" name="realFatherName" value="<%=person.getFatherName()%>"/>
                </td>
                <td><input type="date" class="form-control" name="birthDate" value="<%=person.getBirthDate()%>"/>
                </td>
                <td><input type="text" class="form-control" name="nationalCode" maxlength="10" minlength="8" value="<%=person.getNationalCode()%>"/>
                </td>
                <td><input type="text" class="form-control" name="realCustomerNumber" value="<%=person.getCustomerNumber()%>" readonly/>
                </td>
                <td><input type="submit" class="btn btn-info" value="UPDATE"/></td>
            </form>
            <td><input type="button" value="delete" class="btn btn-danger"
                       onclick="deletePerson(<%=person.getNationalCode()%>)"></td>
        </tr>
        <%
            }
        %>
        </tbody .table-hover>
    </table>
</div>
<script>
    function deletePerson(number) {
        window.location = "/realPerson/disable.do?nationalCode=" + number ;
    }
</script>
</body>