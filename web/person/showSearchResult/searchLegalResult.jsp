<%@ page import="model.entity.LegalPerson" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>customer list show room</title>
    <link rel="stylesheet" href="/WEB-INF/bootstrap-4.1.3">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>RegisterDate</th>
            <th>EconomicCode</th>
            <th>CustomerNumber</th>
            <th>ACTION</th>
            <th>ACTION</th>
        </tr>
        </thead>
        <tbody>
        <%
            List<LegalPerson> legalPersonList = (List<LegalPerson>) request.getAttribute("result");
            for (LegalPerson person : legalPersonList) {
        %>
        <tr>
            <form action="/update.do">
                <td><input type="text" class="form-control" name="legalName" value="<%=person.getName()%>"/>
                </td>
                <td><input type="date" class="form-control" name="registerDate" value="<%=person.getRegisterDate()%>"/>
                </td>
                <td><input type="text" class="form-control" name="economicCode" maxlength="12" minlength="12"
                           value="<%=person.getEconomicCode()%>"/>
                </td>
                <td><input type="text" class="form-control" name="legalCustomerNumber"
                           value="<%=person.getCustomerNumber()%>" readonly/>
                </td>
                <td><input type="submit" class="btn btn-info" value="UPDATE"/></td>
            </form>
            <td><input type="button" value="Disable" class="btn btn-danger"
                       onclick="disablePerson(<%=person.getEconomicCode()%>)"></td>
        </tr>
        <%
            }
        %>
        </tbody .table-hover>
    </table>
</div>
<script>
    function disablePerson(number) {
        window.location = "/disable.do?economicCode=" + number;
    }
</script>
</body>