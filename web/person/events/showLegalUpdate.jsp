<%@ page import="model.entity.LegalPerson" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>updated customer showing room</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<h1>Update Done</h1>
<br>
<br>
<div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>RegisterDate</th>
            <th>EconomicCode</th>
            <th>customerNumber</th>
        </tr>
        </thead>
        <tbody>
        <%
            LegalPerson legalPerson = (LegalPerson) request.getAttribute("updateResult");
        %>
        <tr>
            <td><%=legalPerson.getName()%>
            </td>
            <td><%=legalPerson.getRegisterDate()%>
            </td>
            <td><%=legalPerson.getEconomicCode()%>
            </td>
            <td><%=legalPerson.getCustomerNumber()%>
            </td>
        </tr>
        </tbody .table-hover>
    </table>
</div>
<form action="/person/searchCustomers/searchCustomers.jsp">
    <button type="submit" class="btn btn-primary">Home</button>
</form>
</body>