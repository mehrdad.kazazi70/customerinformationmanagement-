<%@ page import="model.entity.LegalPerson" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>new customer showing room</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>registerDate</th>
            <th>EconomicCode</th>
            <th>CustomerNumber</th>
        </tr>
        </thead>
        <tbody>
        <%
            LegalPerson legalPerson = (LegalPerson) request.getAttribute("result");
        %>
        <tr>
            <td><%=legalPerson.getName()%>
            </td>
            <td><%=legalPerson.getRegisterDate()%>
            </td>
            <td><%=legalPerson.getEconomicCode()%>
            </td>
            <td><%=legalPerson.getCustomerNumber()%>
            </td>
        </tr>
        </tbody .table-hover>
    </table>
</div>
<form action="/person/definitionCustomer/definitionProfile.jsp">
    <button type="submit" class="btn btn-primary">back</button>
</form>
</body>