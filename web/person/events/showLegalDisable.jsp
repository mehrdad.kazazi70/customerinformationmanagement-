<%@ page import="model.entity.LegalPerson" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>disable customer showing room</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<h1>Customer Disabled</h1>
<br>
<br>
<div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>RegisterDate</th>
            <th>EconomicCode</th>
            <th>customerNumber</th>
        </tr>
        </thead>
        <tbody>
        <%
           List<LegalPerson> legalPerson = (List<LegalPerson>) request.getAttribute("disableLegalResult");
           for (LegalPerson list:legalPerson){
        %>
        <tr>
            <td><%=list.getName()%>
            </td>
            <td><%=list.getRegisterDate()%>
            </td>
            <td><%=list.getEconomicCode()%>
            </td>
            <td><%=list.getCustomerNumber()%>
            </td>
        </tr>
        <%
            }
        %>
        </tbody .table-hover>
    </table>
</div>
<form action="/person/searchCustomers/searchCustomers.jsp">
    <button type="submit" class="btn btn-primary">Home</button>
</form>
</body>