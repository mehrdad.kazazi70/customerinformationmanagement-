package controller.legalPerson;

import model.entity.LegalPerson;
import model.service.LegalPersonService;
import model.service.formateditor.Editor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

/**
 * By pressing the update button in each row after searching , the element of records have been change by referer to CustomerNumber for each row
 */
@WebServlet("/update.do")
public class UpdateLegal extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp){
        try {
            LegalPerson legalPerson = new LegalPerson();
            legalPerson.setName(req.getParameter("legalName"));
            legalPerson.setEconomicCode(req.getParameter("economicCode"));
            legalPerson.setRegisterDate(Editor.converter(req.getParameter("registerDate")));
            legalPerson.setCustomerNumber(Long.valueOf(req.getParameter("legalCustomerNumber")));
            LegalPersonService.getInstance().update(legalPerson);
            req.setAttribute("updateResult",legalPerson);
            req.getRequestDispatcher("/person/events/showLegalUpdate.jsp").forward(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
