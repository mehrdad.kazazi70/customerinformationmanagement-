package controller.legalPerson;

import model.entity.LegalPerson;
import model.service.LegalPersonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * By pressing the deactivation button in each row, the record by referer to economicCode status is changed to inactive
 */
@WebServlet("/disable.do")
public class DisableLegal extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) {
        LegalPerson legalPerson = new LegalPerson();
        legalPerson.setEconomicCode(req.getParameter("economicCode"));
        legalPerson.setStatus(0);
        LegalPersonService.getInstance().delete(legalPerson);
        try {
            req.setAttribute("disableLegalResult", LegalPersonService.getInstance().findAll(req.getParameter("economicCode")));
            req.getRequestDispatcher("/person/events/showLegalDisable.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

