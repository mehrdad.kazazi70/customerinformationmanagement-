package controller.legalPerson;

import model.entity.LegalPerson;
import model.service.LegalPersonService;
import model.service.formateditor.Editor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * Receive input and registration information in the database after validation
 *
 */
@WebServlet("/saveLegal.do")
public class SaveLegal extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(SaveLegal.class));
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LegalPerson legalPerson = new LegalPerson();
        legalPerson.setStatus(1);
        legalPerson.setName(req.getParameter("legalName"));
        try {
            legalPerson.setRegisterDate(Editor.converter(req.getParameter("registerDate")));
            legalPerson.setEconomicCode(req.getParameter("legalEconomicCode"));
            if (legalPerson.getEconomicCode().equals("000000000000")) {
                try {
                    legalPerson.setStatus(0);
                    LOGGER.info("input zero value");
                    resp.sendError(707);
                    throw new Exception("invalid NationalCode");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            legalPerson.setCustomerNumber(Long.valueOf(Objects.requireNonNull(Editor.randomNumberProducer(String.valueOf(legalPerson.getEconomicCode())))));
            if (legalPerson.getCustomerNumber() == 0) {
                legalPerson.setStatus(0);
                resp.sendError(707);
                throw new Exception("input economicCode is invalid");
            }
            try {
                LegalPersonService.getInstance().save(legalPerson);
                req.setAttribute("result", legalPerson);
                req.getRequestDispatcher("/person/events/insertCustomer/resultLegalAdd.jsp").forward(req, resp);
            } catch (Exception e) {
                List<LegalPerson> checkList = LegalPersonService.getInstance().findAll(legalPerson.getEconomicCode());
                for (LegalPerson list : checkList) {
                    if (list.getEconomicCode().equals(legalPerson.getEconomicCode()) && list.getStatus() == 0) {
                        LegalPersonService.getInstance().activeAccount(legalPerson.getEconomicCode());
                        resp.sendError(707);
                    }
                }
                LegalPersonService.getInstance().activeAccount(legalPerson.getEconomicCode());
                try {
                    resp.sendError(500);
                    throw new Exception("Duplicate nationalCode");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
