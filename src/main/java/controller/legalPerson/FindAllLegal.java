package controller.legalPerson;

import model.entity.LegalPerson;
import model.service.LegalPersonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *Customer information is retrieved based on each field
 */
@WebServlet("/findAll.do")
public class FindAllLegal extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LegalPerson legalPerson = new LegalPerson();
        legalPerson.setName(req.getParameter("legalName"));
        if (req.getParameter("economicCode").equals("")) {
            legalPerson.setEconomicCode(String.valueOf(0));
        } else legalPerson.setEconomicCode(req.getParameter("economicCode"));
        if (req.getParameter("customerNumber").equals("")) {
            legalPerson.setCustomerNumber((long) 0);
        } else legalPerson.setCustomerNumber(Long.valueOf(req.getParameter("customerNumber")));
        try {
            req.setAttribute("result", LegalPersonService.getInstance().selectAll(legalPerson));
            req.getRequestDispatcher("/person/showSearchResult/searchLegalResult.jsp").forward(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
