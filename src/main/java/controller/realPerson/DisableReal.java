package controller.realPerson;

import model.entity.RealPerson;
import model.service.RealPersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * By pressing the deactivation button in each row, the record by referer to nationalCode status is changed to inactive
 */
public class DisableReal extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RealPerson realPerson = new RealPerson();
        realPerson.setNationalCode(req.getParameter("nationalCode"));
        realPerson.setStatus(0);
        RealPersonService.getInstance().delete(realPerson);

        try {
            req.setAttribute("disableRealResult",RealPersonService.getInstance().findAll(req.getParameter("nationalCode")));
            req.getRequestDispatcher("/person/events/showRealDisable.jsp").forward(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
