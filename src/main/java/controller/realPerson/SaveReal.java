package controller.realPerson;

import model.entity.RealPerson;
import model.service.RealPersonService;
import model.service.formateditor.Editor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
/**
 * Receive input and registration information in the database after validation
 *
 */
public class SaveReal extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) {
        RealPerson realPerson = new RealPerson();
        realPerson.setName(req.getParameter("name"));
        realPerson.setFamily(req.getParameter("family"));
        realPerson.setFatherName(req.getParameter("fatherName"));
        realPerson.setStatus(1);
        try {
            realPerson.setBirthDate(Editor.converter(req.getParameter("birthDate")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        realPerson.setNationalCode(req.getParameter("nationalCode"));
        if (realPerson.getNationalCode().equals("0000000000")) {
                realPerson.setStatus(0);
            try {
                throw new Exception("invalid NationalCode");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            realPerson.setCustomerNumber(Editor.randomNumberProducer(req.getParameter("nationalCode")));
        if (realPerson.getCustomerNumber().equals(String.valueOf(0))) {
            realPerson.setStatus(0);
                throw new Exception("input nationalCode is invalid");
        }
        try {
            RealPersonService.getInstance().save(realPerson);
            req.setAttribute("result", realPerson);
            req.getRequestDispatcher("/person/events/insertCustomer/resultRealAdd.jsp").forward(req, resp);
        } catch (Exception e) {
            //here ****
            try {
                List<RealPerson> checkList = RealPersonService.getInstance().findAll(realPerson.getNationalCode());
                for (RealPerson list : checkList) {
                    if (list.getNationalCode().equals(realPerson.getNationalCode())&& list.getStatus()==0) {
                        RealPersonService.getInstance().activeAccount(realPerson.getNationalCode());
                        resp.sendError(500);
                        throw new Exception("Duplicate nationalCode");
                    }throw new Exception("Duplicate nationalCode");
                }
            } catch (Exception ex) {
                try {
                    resp.sendError(707);
                } catch (IOException exc) {
                    exc.printStackTrace();
                }
                ex.printStackTrace();
            }
        }
    } catch (Exception e) {
            e.printStackTrace();
        }
    }
}