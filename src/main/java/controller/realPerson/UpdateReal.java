package controller.realPerson;

import model.entity.RealPerson;
import model.service.RealPersonService;
import model.service.formateditor.Editor;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * By pressing the update button in each row after searching , the element of records have been change by referer to CustomerNumber for each row
 */
public class UpdateReal extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) {
        try {
            RealPerson realPerson = new RealPerson();
            realPerson.setName(req.getParameter("realName"));
            realPerson.setFamily(req.getParameter("realFamily"));
            realPerson.setFatherName(req.getParameter("realFatherName"));
            realPerson.setBirthDate(Editor.converter(req.getParameter("birthDate")));
            realPerson.setNationalCode(req.getParameter("nationalCode"));
            realPerson.setCustomerNumber(req.getParameter("realCustomerNumber"));
            RealPersonService.getInstance().update(realPerson);
            req.setAttribute("updateResult", realPerson);
            req.getRequestDispatcher("/person/events/showRealUpdate.jsp").forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
            try {
                resp.sendError(707);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
