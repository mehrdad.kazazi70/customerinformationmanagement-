package controller.realPerson;

import model.entity.RealPerson;
import model.service.RealPersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Customer information is retrieved based on each field
 */
public class FindAllReal extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            RealPerson realPerson = new RealPerson();
            realPerson.setName(req.getParameter("realName"));
            realPerson.setFamily(req.getParameter("realFamily"));
            if (req.getParameter("realNationalCode").equals("")) {
                realPerson.setNationalCode(String.valueOf(0));
            }else realPerson.setNationalCode(req.getParameter("realNationalCode"));
            if (req.getParameter("realCustomerNumber").equals("")) {
                realPerson.setCustomerNumber(String.valueOf(0));
            }else realPerson.setCustomerNumber(req.getParameter("realCustomerNumber"));
            req.setAttribute("result", RealPersonService.getInstance().selectAll(realPerson));
            req.getRequestDispatcher("/person/showSearchResult/searchRealResult.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendError(707);
        }
    }
}
