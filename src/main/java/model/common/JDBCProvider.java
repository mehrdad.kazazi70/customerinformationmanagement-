package model.common;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Connect to the database and manage the number of simultaneous connections to the database
 */
public class JDBCProvider {
    private static BasicDataSource basicDataSource  = new BasicDataSource();
    static
    {
        basicDataSource.setMaxTotal(10);
        basicDataSource.setPassword("myjava123");
        basicDataSource.setUsername("dotin");
        basicDataSource.setUrl("jdbc:oracle:thin:@localhost:1521:orcl");
        basicDataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
    }
    public static Connection getConnection() throws SQLException {
        return basicDataSource.getConnection();
    }
}
