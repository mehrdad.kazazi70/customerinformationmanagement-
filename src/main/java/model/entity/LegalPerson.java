package model.entity;

import java.util.Date;

/**
 * The schema of a realPerson table was created
 */
public class LegalPerson {
    private Long id;
    private String name;
    private Date registerDate;
    private String economicCode;
    private int status;
    private Long CustomerNumber;

    public LegalPerson() {
    }

    public LegalPerson(String name, Date registerDate, String economicCode, int status, Long customerNumber) {
        this.name = name;
        this.registerDate = registerDate;
        this.economicCode = economicCode;
        this.status = status;
        CustomerNumber = customerNumber;
    }

    public LegalPerson(Long id, String name, Date registerDate, String economicCode, int status, Long customerNumber) {
        this.id = id;
        this.name = name;
        this.registerDate = registerDate;
        this.economicCode = economicCode;
        this.status = status;
        CustomerNumber = customerNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getEconomicCode() {
        return economicCode;
    }

    public void setEconomicCode(String economicCode) {
        this.economicCode = economicCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCustomerNumber() {
        return CustomerNumber;
    }

    public void setCustomerNumber(Long customerNumber) {
        CustomerNumber = customerNumber;
    }
}
