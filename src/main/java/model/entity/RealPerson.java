package model.entity;

import java.util.Date;

/**
 * The schema of a realPerson table was created
 */

public class RealPerson {
    private Long id;
    private String name;
    private String family;
    private String fatherName;
    private Date birthDate;
    private String nationalCode;
    private int status;
    private String customerNumber;

    public RealPerson() {
    }

    public RealPerson(String name, String family, String fatherName, Date birthDate, String nationalCode, int status, String customerNumber) {
        this.name = name;
        this.family = family;
        this.fatherName = fatherName;
        this.birthDate = birthDate;
        this.nationalCode = nationalCode;
        this.status = status;
        this.customerNumber = customerNumber;
    }

    public RealPerson(Long id, String name, String family, String fatherName, Date birthDate, String nationalCode, int status, String customerNumber) {
        this.id = id;
        this.name = name;
        this.family = family;
        this.fatherName = fatherName;
        this.birthDate = birthDate;
        this.nationalCode = nationalCode;
        this.status = status;
        this.customerNumber = customerNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }
}

