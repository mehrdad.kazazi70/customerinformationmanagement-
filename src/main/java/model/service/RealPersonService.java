package model.service;

import model.entity.RealPerson;
import model.repository.RealPersonDA;

import java.sql.SQLException;
import java.util.List;

public class RealPersonService {
    private static RealPersonService ourInstance = new RealPersonService();

    private RealPersonService() {
    }

    public static RealPersonService getInstance() {
        return ourInstance;
    }

    public void save(RealPerson realPerson) throws Exception {
        try (RealPersonDA realPersonDA = new RealPersonDA()) {
            realPersonDA.insert(realPerson);
        }
    }

    public void update(RealPerson realPerson) {
        try (RealPersonDA realPersonDA = new RealPersonDA()) {
            realPersonDA.update(realPerson);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void activeAccount(String nationalCode) {
        try (RealPersonDA realPersonDA = new RealPersonDA()) {
            realPersonDA.activeAccount(nationalCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(RealPerson realPerson) {
        try (RealPersonDA realPersonDA = new RealPersonDA()) {
            realPersonDA.delete(realPerson);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<RealPerson> selectAll(RealPerson realPerson) throws Exception {
        try (RealPersonDA realPersonDA = new RealPersonDA()) {
            return realPersonDA.select(realPerson);
        }
    }
    public List<RealPerson> findAll(String nationalCode) throws Exception {
        try (RealPersonDA realPersonDA = new RealPersonDA()) {
            return realPersonDA.findAll(nationalCode);
        }
    }
}
