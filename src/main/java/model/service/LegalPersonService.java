package model.service;

import model.entity.LegalPerson;
import model.repository.LegalPersonDA;
import model.repository.RealPersonDA;

import java.util.List;

public class LegalPersonService {
    private static LegalPersonService ourInstance = new LegalPersonService();

    private LegalPersonService() {
    }

    public static LegalPersonService getInstance() {
        return ourInstance;
    }

    public void save(LegalPerson legalPerson) throws Exception {
        try (LegalPersonDA legalPersonDA = new LegalPersonDA()) {
            legalPersonDA.insert(legalPerson);
        }
    }
    public void activeAccount(String economicCode) {
        try (LegalPersonDA legalPersonDA = new LegalPersonDA()) {
            legalPersonDA.activeAccount(economicCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<LegalPerson> selectAll(LegalPerson legalPerson) throws Exception {
        try (LegalPersonDA legalPersonDA = new LegalPersonDA()) {
            return legalPersonDA.selectAll(legalPerson);
        }
    }
    public List<LegalPerson> findAll(String economicCode) throws Exception {
        try (LegalPersonDA legalPersonDA = new LegalPersonDA()) {
            return legalPersonDA.findAll(economicCode);
        }
    }

    public void delete(LegalPerson legalPerson) {
        try {
            try (LegalPersonDA legalPersonDA = new LegalPersonDA()) {
                legalPersonDA.delete(legalPerson);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update(LegalPerson legalPerson) throws Exception {
        try (LegalPersonDA legalPersonDA = new LegalPersonDA()) {
            legalPersonDA.update(legalPerson);
        }
    }
}
