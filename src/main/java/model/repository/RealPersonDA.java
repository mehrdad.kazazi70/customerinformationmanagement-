package model.repository;


import model.common.JDBCProvider;
import model.entity.RealPerson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Orders for database affairs are issued to the database in accordance with the requirements
 */
public class RealPersonDA implements AutoCloseable {
    private Connection connection;
    private PreparedStatement preparedStatement;

    public RealPersonDA() {
        try {
            connection = JDBCProvider.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(RealPerson realPerson) throws SQLException {
        preparedStatement = connection.prepareStatement("select person_seq.nextval nid from dual");
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        realPerson.setId(resultSet.getLong("nid"));
        preparedStatement = connection.prepareStatement("insert into realperson(id,name,family,fathername,birthdate,nationalcode,status,customernumber)values(?,?,?,?,?,?,?,?)");
        preparedStatement.setLong(1, realPerson.getId());
        preparedStatement.setString(2, realPerson.getName());
        preparedStatement.setString(3, realPerson.getFamily());
        preparedStatement.setString(4, realPerson.getFatherName());
        preparedStatement.setDate(5, new java.sql.Date(realPerson.getBirthDate().getTime()));
        preparedStatement.setLong(6, Long.parseLong(realPerson.getNationalCode()));
        preparedStatement.setInt(7, realPerson.getStatus());
        preparedStatement.setLong(8, Long.parseLong(realPerson.getCustomerNumber()));
        preparedStatement.executeUpdate();
    }

    public void update(RealPerson realPerson) throws SQLException {
        preparedStatement = connection.prepareStatement("update realperson set name=?,family=?,fathername=?,birthdate=?,nationalcode=? where customernumber=?");
        preparedStatement.setString(1, realPerson.getName());
        preparedStatement.setString(2, realPerson.getFamily());
        preparedStatement.setString(3, realPerson.getFatherName());
        preparedStatement.setDate(4, new java.sql.Date(realPerson.getBirthDate().getTime()));
        preparedStatement.setLong(5, Long.parseLong(realPerson.getNationalCode()));
        preparedStatement.setLong(6, Long.parseLong(realPerson.getCustomerNumber()));
        preparedStatement.executeUpdate();
    }
    public void activeAccount(String nationalCode) throws SQLException {
        preparedStatement = connection.prepareStatement("update realperson set status=1 where nationalcode=?");
        preparedStatement.setLong(1, Long.parseLong(nationalCode));
        preparedStatement.executeUpdate();
    }

    public void delete(RealPerson realPerson) throws SQLException {
        preparedStatement = connection.prepareStatement("update realperson set status=? where nationalcode=?");
        preparedStatement.setInt(1, realPerson.getStatus());
        preparedStatement.setLong(2, Long.parseLong(realPerson.getNationalCode()));
        preparedStatement.executeUpdate();
    }

    public List<RealPerson> select(RealPerson person) throws Exception {
        preparedStatement = connection.prepareStatement("select * from realperson where status=1 AND (name=? or family=? or nationalcode=? or customernumber=?) ");
        preparedStatement.setString(1, person.getName());
        preparedStatement.setString(2, person.getFamily());
        preparedStatement.setLong(3, Long.parseLong(person.getNationalCode()));
        preparedStatement.setLong(4, Long.parseLong(person.getCustomerNumber()));
        ResultSet resultSet = preparedStatement.executeQuery();
        List<RealPerson> realPersonList = new ArrayList<>();
        while (resultSet.next()) {
            RealPerson realPerson = new RealPerson();
            realPerson.setId(resultSet.getLong("ID"));
            realPerson.setName(resultSet.getString("NAME"));
            realPerson.setFamily(resultSet.getString("FAMILY"));
            realPerson.setFatherName(resultSet.getString("FATHERNAME"));
            realPerson.setBirthDate(resultSet.getDate("BIRTHDATE"));
            realPerson.setNationalCode(String.valueOf(resultSet.getLong("NATIONALCODE")));
            realPerson.setStatus(resultSet.getInt("STATUS"));
            realPerson.setCustomerNumber(String.valueOf(resultSet.getLong("CUSTOMERNUMBER")));
            realPersonList.add(realPerson);
        }
        return realPersonList;
    }
    public List<RealPerson> findAll(String nationalCode) throws Exception {
        preparedStatement = connection.prepareStatement("select * from realperson where nationalCode=? ");
        preparedStatement.setLong(1, Long.parseLong(nationalCode));
        ResultSet resultSet = preparedStatement.executeQuery();
        List<RealPerson> list = new ArrayList<>();
        while (resultSet.next()) {
            RealPerson realPerson = new RealPerson();
            realPerson.setId(resultSet.getLong("ID"));
            realPerson.setName(resultSet.getString("NAME"));
            realPerson.setFamily(resultSet.getString("FAMILY"));
            realPerson.setFatherName(resultSet.getString("FATHERNAME"));
            realPerson.setBirthDate(resultSet.getDate("BIRTHDATE"));
            realPerson.setNationalCode(String.valueOf(resultSet.getLong("NATIONALCODE")));
            realPerson.setStatus(resultSet.getInt("STATUS"));
            realPerson.setCustomerNumber(String.valueOf(resultSet.getLong("CUSTOMERNUMBER")));
            list.add(realPerson);
        }
        return list;
    }

    public void close() throws Exception {
        connection.commit();
        preparedStatement.close();
        connection.close();
    }
}
