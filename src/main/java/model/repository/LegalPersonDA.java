package model.repository;

import model.common.JDBCProvider;
import model.entity.LegalPerson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Orders for database affairs are issued to the database in accordance with the requirements
 */
public class LegalPersonDA implements AutoCloseable {
    private Connection connection;
    private PreparedStatement preparedStatement;

    public LegalPersonDA() {
        try {
            connection = JDBCProvider.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(LegalPerson legalPerson) throws SQLException {
        preparedStatement = connection.prepareStatement("select person_seq.nextval nid from dual");
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        legalPerson.setId(resultSet.getLong("nid"));
        preparedStatement = connection.prepareStatement("insert into legalperson(id,name,registerdate,economiccode,status,customernumber) values (?,?,?,?,?,?)");
        preparedStatement.setLong(1, legalPerson.getId());
        preparedStatement.setString(2, legalPerson.getName());
        preparedStatement.setDate(3, new java.sql.Date(legalPerson.getRegisterDate().getTime()));
        preparedStatement.setLong(4, Long.parseLong(legalPerson.getEconomicCode()));
        preparedStatement.setInt(5, legalPerson.getStatus());
        preparedStatement.setLong(6, legalPerson.getCustomerNumber());
        preparedStatement.executeUpdate();
    }

    public List<LegalPerson> selectAll(LegalPerson person) throws Exception {
        preparedStatement = connection.prepareStatement("select * from legalperson where status=1 AND (name=? or economiccode=? or customernumber=?) ");
        preparedStatement.setString(1, person.getName());
        preparedStatement.setString(2, person.getEconomicCode());
        preparedStatement.setLong(3, person.getCustomerNumber());
        ResultSet resultSet = preparedStatement.executeQuery();
        List<LegalPerson> legalPersonList = new ArrayList<>();
        while (resultSet.next()) {
            LegalPerson legalPerson = new LegalPerson();
            legalPerson.setId(resultSet.getLong("ID"));
            legalPerson.setName(resultSet.getString("NAME"));
            legalPerson.setRegisterDate(resultSet.getDate("REGISTERDATE"));
            legalPerson.setEconomicCode(resultSet.getString("ECONOMICCODE"));
            legalPerson.setCustomerNumber(resultSet.getLong("CUSTOMERNUMBER"));
            legalPerson.setStatus(resultSet.getInt("STATUS"));
            legalPersonList.add(legalPerson);
        }
        return legalPersonList;
    }
    public List<LegalPerson> findAll(String economicCode) throws Exception {
        preparedStatement = connection.prepareStatement("select * from legalperson where economicCode=?");
        preparedStatement.setLong(1, Long.parseLong(economicCode));
        ResultSet resultSet = preparedStatement.executeQuery();
        List<LegalPerson> legalPersonList = new ArrayList<>();
        while (resultSet.next()) {
            LegalPerson legalPerson = new LegalPerson();
            legalPerson.setId(resultSet.getLong("ID"));
            legalPerson.setName(resultSet.getString("NAME"));
            legalPerson.setRegisterDate(resultSet.getDate("REGISTERDATE"));
            legalPerson.setEconomicCode(resultSet.getString("ECONOMICCODE"));
            legalPerson.setCustomerNumber(resultSet.getLong("CUSTOMERNUMBER"));
            legalPerson.setStatus(resultSet.getInt("STATUS"));
            legalPersonList.add(legalPerson);
        }
        return legalPersonList;
    }

    public void delete(LegalPerson legalPerson) throws SQLException {
        preparedStatement = connection.prepareStatement("update legalperson set status=? where economiccode=?");
        preparedStatement.setInt(1, legalPerson.getStatus());
        preparedStatement.setLong(2, Long.parseLong(legalPerson.getEconomicCode()));
        preparedStatement.executeUpdate();
    }

    public void update(LegalPerson legalPerson) throws SQLException {
        preparedStatement = connection.prepareStatement("update legalperson set name=?,registerdate=?,economiccode=? where customernumber=?");
        preparedStatement.setString(1, legalPerson.getName());
        preparedStatement.setDate(2, new java.sql.Date(legalPerson.getRegisterDate().getTime()));
        preparedStatement.setLong(3, Long.parseLong(legalPerson.getEconomicCode()));
        preparedStatement.setLong(4, legalPerson.getCustomerNumber());
        preparedStatement.executeUpdate();
    }
    public void activeAccount(String economicCode) throws SQLException {
        preparedStatement = connection.prepareStatement("update legalperson set status=1 where economicCode=?");
        preparedStatement.setLong(1, Long.parseLong(economicCode));
        preparedStatement.executeUpdate();
    }

    @Override
    public void close() throws Exception {
        connection.commit();
        preparedStatement.close();
        connection.close();
    }
}
